Tools for engineering students
============================

With the advent of technology, engineering has changed and being a student, I have to look for ways to meet the demands of passing the exams and doing better in projects. With the help of the software tools from the web I was able to organize and simplify my works. Here are the lists of tools that I’ve found helpful: ElectroDroid, High Voltage Engineering, Electrician’s Bible, EE Engineer’s Handbook, LTpowerPlay, Logisim, Design Dimensions Pro, [Pokeroriental](http://poker.oriental303.com/), , Cable 3D, Electronic Toolbox Pro, Engineering Calc.

I’m so thankful that my friend has mentioned of using these tools because since then loads of work has been done easily and without delay.



Design Dimensions Pro

![Screenshot of 2D](http://m.eet.com/media/1170769/fig2large.jpg)

Power eSim

![Screenshot of 2D](http://www.electronics-lab.com/wp-content/uploads/2012/03/PowerEsim.png)